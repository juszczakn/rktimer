# rktimer

Just a simple timer. I use it when making tea :D

Requires libnotify package for notifications.

`raco package install libnotify`
