#lang racket

(require
 racket/gui
 racket/contract
 pict)
(require "notification.rkt")

(define frame
  (new frame%
       [label "Timer"]))
(define vbox
  (new vertical-panel%
       [parent frame]))
(define hbox
  (new horizontal-panel%
       [parent vbox]))
(define min-field
  (new text-field%
       [label "M:"]
       [parent hbox]
       [init-value "0"]))
(define sec-field
  (new text-field%
       [label "S:"]
       [parent hbox]
       [init-value "0"]))

(struct alarm ((timer #:mutable) m s)
  #:guard (struct-guard/c (is-a?/c timer%) real? real?))

(define/contract *alarm*
  (or/c false? alarm?)
  #f)

(define (update-ui-timer-cb!)
  (let ([m (string->number (send min-field get-value))]
        [s (string->number (send sec-field get-value))])
    (match (list m s)
      [(list 0 0) (let ()
                    (stop-alarm!)
                    (send-notification! "Timer Finished!" 3)
                    (bell)
                    (set-alarm-timer! *alarm*
                                      (new timer%
                                           [notify-callback
                                            (λ ()
                                              (send-notification! "Timer Finished!" 3)
                                              (bell))]
                                           [interval 5000]
                                           [just-once? #f])))]
      [(list _ 0) (let ()
                    (send sec-field set-value "59")
                    (send min-field set-value (number->string (- m 1))))]
      [(list _ _) (send sec-field set-value (number->string (- s 1)))])))


(define (start-alarm!)
  (when *alarm*
    (stop-alarm!))
  (let ([m (string->number (send min-field get-value))]
        [s (string->number (send sec-field get-value))])
    (unless (or (or m s) (> 0 (+ m s)))
      (error 'no-time-set))
    (send min-field enable #f)
    (send sec-field enable #f)
    (unless m (send min-field set-value "0"))
    (unless s (send sec-field set-value "0"))
    (let ([timer (new timer%
                      [notify-callback update-ui-timer-cb!]
                      [interval 1000]
                      [just-once? #f])])
      (set! *alarm* (alarm timer (or m 0) (or s 0))))))

(define (stop-alarm!)
  (send min-field enable #t)
  (send sec-field enable #t)
  (send (alarm-timer *alarm*) stop))

(define (reset-alarm!)
  (when *alarm*
    (stop-alarm!)
    (send min-field set-value (number->string (alarm-m *alarm*)))
    (send sec-field set-value (number->string (alarm-s *alarm*)))))

(define (clear-alarm!)
  (stop-alarm!)
  (send min-field set-value "0")
  (send sec-field set-value "0"))


(define hbox2
  (new horizontal-panel%
       [parent vbox]))
(define start
  (new button%
       [label "Start"]
       [parent hbox2]
       [callback (λ (b e) (start-alarm!))]))
(define stop
  (new button%
       [label "Stop"]
       [parent hbox2]
       [callback (λ (b e) (stop-alarm!))]))
(define clear
  (new button%
       [label "Clear"]
       [parent hbox2]
       [callback (λ (b e) (clear-alarm!))]))
(define reset
  (new button%
       [label "Reset"]
       [parent hbox2]
       [callback (λ (b e) (reset-alarm!))]))


(define (main)
  (send frame set-icon (pict->bitmap (bitmap "rktimer.png")))
  (send frame show #t))
(main)
