#lang racket

(provide send-notification!)

(require
 libnotify)

(define/contract (send-notification! summary timeout-sec)
  (-> string? exact-nonnegative-integer? void?)
  (send (new notification%
         [summary summary]
         [timeout timeout-sec])
        show))
